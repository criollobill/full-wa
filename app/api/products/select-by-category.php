<?php
$id_category = @$_POST['id_category'];

if(!$id_category){
    return Err('IDS_CATEGORY_NULL');
}
return SqlQuery("SELECT p.*, GROUP_CONCAT(DISTINCT pi.image ORDER BY sort SEPARATOR \"!@!\") as images FROM products p LEFT JOIN product_categories pc on pc.id_product = p.id LEFT JOIN product_images pi on pi.id_product = p.id WHERE pc.id_category in ($id_category) and p.active = 1 GROUP BY p.id ORDER BY p.id");