<?php
$id_product = @$_POST['id_product'];

if(!$id_product){
    return Err('ID_PRODUCT_NULL');
}

$data = [];
$arr_product = SqlSelect('products', "id = $id_product");

if(!count($arr_product)){
    return Err('PRODUCT_NOT_FOUND');
}

$data['products'] = $arr_product[0];
$data['product_categories'] = SqlQuery("SELECT c.* FROM categories c LEFT JOIN product_categories pc on pc.id_category = c.id WHERE pc.id_product = $id_product ORDER BY sort");
$data['product_images'] = SqlSelect('product_images', "id_product = $id_product ORDER BY sort");

return $data;
