class cProductList{
    constructor($stateParams, $rootScope, Post, ngMeta){
        this.stateParams = $stateParams;
        this.rootScope = $rootScope;
        this.Post = Post;
        this.ngMeta = ngMeta;

        this.link = this.stateParams.link;

        this.listLinks = {};
        this.categories = [];
        this.products = [];

        this.init();
    }

    init(){
        this.Post('categories/select-all').then(res => {
            res.forEach(category => {
                this.listLinks[category.link] = category.id;
            });

            //save current link id
            sessionStorage.productList_idCategory = this.listLinks[this.link] || 0;

            if(this.link === 'todos'){
                const multipleIds = Object.values(this.listLinks).join(',');
                this.getProducts(multipleIds);
            }else{
                const singleId = this.listLinks[this.link];
                this.getProducts(singleId);
            }

            const title = this.link === 'todos' ? 'Todos' : searchArray(res, 'link', this.link).name;
            this.ngMeta.setTitle(`Categoria: ${title}`);
            this.ngMeta.setTag('description', `Você está os produtos da categoria ${title} de nosso catálogo.`);


            this.categories = resolveStructure(res, 'id_category');
        }, err => {
            console.log('err', err);
        })
    }

    getProducts(ids){
        this.Post('products/select-by-category', {id_category: ids}).then(res => {
            this.products = res;
        }, err => {
            console.log('err', err);
        })
    }

}