function filterDateUnix(){
    return (date) => {
        return moment(date).unix() * 1000;
    }
}